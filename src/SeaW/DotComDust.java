package SeaW;

import chapter_5.DotCom;
import chapter_5.GameHelper;

import java.util.ArrayList;
public class DotComDust{
    private GameHelper helper = new GameHelper();
    private ArrayList<DotCom> dotComList = new ArrayList<DotCom>//массив для сайтов
    private int numOfGuesses = 0;
    
    private void SetUpGame(){
        //Создаем несколько сайтов, даем им имена и помещаем в массив
        DotCom one = new DotCom();
        one.setName("Pets.com");
        DotCom two = new DotCom();
        two.setName("eToys.com");
        DotCom three = new DotCom();
        three.setName("Go2.com");
        dotComsList.add(one);
        dotComsList.add(two);
        dotComsList.add(three);
        
    //Выводим краткие инструкции для пользователя
    System.out.println("Ваша цель три сайта");
    System.out.println("Pets , eToys , Go2");
    System.out.println("Надо потопить их за минимальное количество ходов");
    
    //повторяем с каждым объектом DotCom в списке
        for(DotCom:dotComsList){
            //запрашиваем у вспомогательного объекта адрес сайта
            ArrayList<String> newLocation = helper.placeDotCom(3);
            //Вызываем сеттер из объекта DotCom чтоб передать ему местоположение
            //которое только что получили от вспомогательного объекта
            dotComToSet.setLocationCells(newLocation);
        }
    }
    private void startPlaying(){
        while (!dotComsList.isEmpty()){//до тех пор пока список объектов DotCom  не будет пустым
        String userGuess = helper.getUserInput("Сделайте ход");//Получаем пользовательский ввод
        checkUserGuess(userGuess);//вызываем метод checkUserGuess
        }
        finishGame();
    }
}