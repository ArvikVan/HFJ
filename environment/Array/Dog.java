class Dog{
    String name;
    public static void main(String [] args){
        Dog dog1 = new Dog();//Создаем объект Dog и получаем к нему доступ
        dog1.bark();
        dog1.name = "Барт";
        
        Dog [] myDogs = new Dog[3];//Создаем массив типа Dog
        
        myDogs[0] = new Dog();//помещаем в него несколько элементов
        myDogs[1] = new Dog();
        myDogs[2] = dog1;
        
        myDogs[0].name = "Фред";//получаем доступ к объектам Dog c помощью ссылок (myDogs[1].name) из массива
        myDogs[1].name = "Джордж";
        
        System.out.println("Имя последней собаки - " + myDogs[2].name);
        
        int x = 0;
        while (x < myDogs.length){
            myDogs[x].bark();
            x = x + 1;
        }
    }
    public void bark(){
        System.out.println (name + " сказал Гав!");
        
    }public void eat(){}
    public void chaseCat(){}
}