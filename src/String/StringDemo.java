package String;

class StringDemo{
    public static void main (String [] args){
        String str = new String("Первая строка");
        String str2 = "Вторая строка";
        String str3 = new String(str2);
        
        System.out.println(str);
        System.out.println(str2);
        System.out.println(str3);
    }
}